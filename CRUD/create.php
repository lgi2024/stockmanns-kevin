
<?php
    include './utils/connect-db.php';
    require './services/file-uploader.php';
    require './services/user-services.php';

    $userService = new UserService($con);

    if($userService->showErrorIfNotAdmin()){
        exit();
    }


    if(isset($_POST["name"]) && isset($_POST['age']) && isset($_POST['email'])){

        $file = $_FILES['profile'];
        $name = $_POST['name'];
        $email = $_POST['email'];
        $age = $_POST['age'];

        if($name && $email && $age && $file && $file['error'] == UPLOAD_ERR_OK){
            $stmt = $con->prepare("INSERT INTO students(name, age, email) VALUES(?, ?, ?)");
            if($stmt){
                $stmt->bind_param("sss", $name, $age, $email);
                if($stmt->execute()){
                    $idUser = $con->insert_id;
                    $fileUploader = new FileUploader();
                    $imagen = $fileUploader->upload($file, $idUser);

                    if(!is_string(($imagen))){
                        $err = $fileUploader->getErrorMessage();
                        if($err == ""){
                            $err = "Ocurrio un error al subir la imagen";
                        }
                        header('Location: create.php?error='.$err);
                        exit();
                    }
                    
                    $stmt = $con->prepare("UPDATE students SET profile = ? WHERE id = ?");
                    $stmt->bind_param("si", $imagen, $idUser);
                    if ($stmt->execute()) {
                        header('Location: read.php');
                        exit();
                    } else {
                        header('Location: create.php?error=insert');
                        exit();
                    }
                }else{
                    header('Location: create.php?error=insert');
                    exit();
                }

            }
        }else{
            header('Location: create.php?error=required_data');
            exit();
        }

        $con->close();
    }
?>


<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CRUD (Create)</title>
    <link rel="stylesheet" href="css/index.css">
</head>
<body>
    <main>
        <h1>Crear Alumno Nuevo</h1>
        <form method="post" enctype="multipart/form-data">
            <div class="form-div">
                <label for="name">Nombre</label>
                <input type="text" name="name" id="name">
                <p class="hidden input-error" id="error-name"></p>
            </div>

            <div class="form-div">
                <label for="age">Edad</label>
                <input type="number" name="age" id="age">
                <p class="hidden input-error" id="error-age"></p>

            </div>

            <div class="form-div">
                <label for="email">Correo</label>
                <input type="email" name="email" id="email">
                <p class="hidden input-error" id="error-email"></p>
            </div>

            <div class="form-div">
                <label for="profile">Foto de Perfil</label>
                <input type="file" name="profile" id="profile">
                <p class="hidden input-error" id="error-profile"></p>
            </div>

            <div class="form-div">
                <input id="form-submit" class="btn" type="submit" value="Agregar">
                <p class="form-error hidden" id="form-error"></p>
            </div>
        </form>
    </main>

    <script>
        const d = document;
        d.getElementById('form-submit').addEventListener("click",e=>{
            let $nameInput = d.getElementById('name');
            let $ageInput = d.getElementById('age');
            let $emailInput = d.getElementById('email');
            let valid = true;

            if($nameInput.value.trim() == ""){
                let $nameError = d.getElementById('error-name');
                $nameError.textContent='El nombre es requerido';
                $nameError.classList.remove('hidden');
                valid = false;
            }
            if($ageInput.value.trim() == "" || isNaN($ageInput.value.trim()) || $ageInput.value<=0){
                let $nameError = d.getElementById('error-age');
                $nameError.textContent='La edad ingresada es inválida';
                $nameError.classList.remove('hidden');
                valid = false;
            }
            if($emailInput.value.trim() == ""){
                let $nameError = d.getElementById('error-email');
                $nameError.textContent='El correo es requerido';
                $nameError.classList.remove('hidden');
                valid = false;
            }

            if(!valid)
                e.preventDefault();
        }); 

        d.addEventListener("DOMContentLoaded", e=>{
            let hasError = new URLSearchParams(window.location.search).get('error');
            let $formError = d.getElementById('form-error');
            if(hasError == "required_data"){
                $formError.textContent = 'Todos los datos son requeridos';
                $formError.classList.remove("hidden");
            }else if(hasError == 'error'){
                $formError.textContent = 'Ocurrio un error. Intentalo nuevamente';
                $formError.classList.remove("hidden");
            }else if(hasError == 'insert'){
                $formError.textContent = 'Ocurrio un error al insertar los datos';
                $formError.classList.remove("hidden");
            }else if(hasError){
                $formError.textContent = hasError;
                $formError.classList.remove("hidden");
            }else{
                $formError.classList.add("hidden");
            }
        })
    </script>
</body>
</html>