<?php
    include ('./utils/connect-db.php');
    require ('./services/file-uploader.php');
    require ('./services/user-services.php');

    $userService = new UserService($con);
    if($userService->showErrorIfNotAdmin()){
        exit();
    }

    if(isset($_POST['delete'])){
        if($_POST['delete'] == "SI"){
            $stmt = $con->prepare("SELECT profile FROM students WHERE id = ?");
            if($stmt){
                $stmt->bind_param("i", $_GET["id"]);
                $stmt->execute();
                $stmt->bind_result($profile);
                $stmt->fetch();
                $stmt->close();

                $fileUploader = new FileUploader();
                $fileUploader->delete($profile);

                $stmt = $con->prepare("DELETE FROM students WHERE id = ?");
                if($stmt){
                    $stmt->bind_param("i", $_GET["id"]);
                    if ($stmt->execute()) {
                        header("Location: read.php");
                    }
                    $stmt->close();
                }
            }
        }else{
            header("Location: read.php");
            exit();
        }
    }
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CRUD (Delete)</title>
    <link rel="stylesheet" href="css/index.css">
</head>
<body>
    <main>
        <h1>¿Seguro que deseas elimiar a un alumno?</h1>
        <form method="post">
            <?php
                $stmt = $con->prepare('SELECT * FROM students WHERE id = ?');
                if($stmt){
                    $stmt->bind_param('s', $_GET['id']);
                    $stmt->execute();
                    $result = $stmt->get_result();
                    if($result){
                        $student = $result->fetch_assoc();
                        print '
                        <ul>
                            <li><b>Nombre:</b> ' . $student['name'] . '
                            <li><b>Edad:</b> ' . $student['age'] . '
                            <li><b>Correo:</b> ' . $student['email'] . '
                        </ul>
                        ';
                    }
                }

            ?>
            <input class="btn red" type="submit" value="NO" name="delete">
            <input class="btn" type="submit" value="SI" name="delete">
        </form>
    </main>
</body>
</html>


<?php
    $con->close();
?>