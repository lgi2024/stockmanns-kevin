
<?php
    include './utils/connect-db.php';
    include './utils/create-tables.php';
    require './services/user-services.php';
    $userService = new UserService($con);
    if($userService->isLogin()){
        header('Location: read.php');
        $con->close();
        exit();
    }
    
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        if(isset($_POST['username']) && isset($_POST['password'])){
            $user = $_POST['username'];
            $pass = $_POST['password'];
            if(!$pass || !$user){
                header('Location: index.php?e=required_data');
                exit();
            }

            $res = $userService->login($user, $pass);
            if(!is_string($res)){
                header('Location: index.php');
            }else{
                header('Location: index.php?e='.$res);
            }

            $con->close();
            exit();
        }else{
            header('Location: index.php?e=required_data');
            exit();
        }
    }

        // $userService = new UserService($con);
        // $userService->login("asd", "123");
        // $userService->isLogin();
    ?>



<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Iniciar Sesión</title>
    <link rel="stylesheet" href="css/index.css">
    <style>
        form{
            width: calc(100% - 2rem);
            max-width: 500px;
        }
        .register-invite{
            display: flex;
            gap: .2rem;
        }
    </style>
</head>
<body>
    <form action="" method="POST">
        <h1>Iniciar Sesión</h1>
        <div class="form-div">
            <label for="username">Correo</label>
            <input type="email" id="username" name="username">
            <p class="hidden input-error" id="error-username"></p>
        </div>

        <div class="form-div">
            <label for="password">Clave</label>
            <input type="password" id="password" name="password">
            <p class="hidden input-error" id="error-password"></p>
        </div>

        <div class="form-div">
                <input id="form-submit" class="btn" type="submit" value="Iniciar Sesión">
                <p class="form-error hidden" id="form-error"></p>
            </div>

        <p class="register-invite">¿No tienes una cuenta? Registrate haciendo <a href="register.php">click aquí</a></p>
    </form>
    
    <script>
        document.addEventListener("DOMContentLoaded", e=>{
            let $formError =document.getElementById('form-error');
            let querys = new URLSearchParams(window.location.search);
            let error =querys.get('e');
            if(error){
                $formError.classList.remove('hidden');
                if(error == 'required_data')
                    $formError.textContent = 'Todos los datos son necesarios';
                else if(error == 'required_login'){
                    $formError.textContent = 'Es necesario iniciar sesión para seguir navegando.';
                }else
                    $formError.textContent = error;
            }
            
        });
    </script>
</body>

</html>