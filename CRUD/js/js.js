document.addEventListener("DOMContentLoaded", () => {
  document.getElementById("queryBtn").addEventListener("click", runQuery);
  document.getElementById("query").addEventListener("keyup", runQuery);

  refreshContent();
  PopUpStudent();
  Order()
});

function runQuery(e) {
  let $query = document.getElementById("query");
  let $sort = document.getElementById("sort");
  let $order = document.getElementById("order");

  let url = window.location.origin + window.location.pathname;
  url += `?`;
  if ($query.value) url += `q=${$query.value}`;
  if ($sort.value) {
    if (url.includes("=")) url += "&";
    url += `o=${$sort.value}`;
  }
  if ($order.value) {
    if (url.includes("=")) url += "&";
    url += `order=${
      $order.value == "ASC" || $order.value == "DESC" ? $order.value : "ASC"
    }`;
  }

  if ((e.type == "keyup" && e.key == "Enter") || e.type == "click")
    window.location.href = url;

  if ((e.type = "keyup")) {
    let $studentsItems = document.querySelectorAll(".student-item");

    $studentsItems.forEach((el) => {
      if (e.target.value != "") {
        let name = el.querySelectorAll("td")[1].textContent;
        let email = el.querySelectorAll("td")[3].textContent;
        let searchTerm = e.target.value.trim().toLowerCase();
        
        if (name.toLowerCase().includes(searchTerm) || email.toLowerCase().includes(searchTerm)) {
          el.style.display = "table-row";
        } else {
          el.style.display = "none";
        }
      }else{
        el.style.display = "table-row";
      }
    });
  }
}

function getParams() {
  let params = {};
  let searchParams = new URLSearchParams(window.location.search);

  searchParams.forEach((value, key) => {
    params[key] = value;
  });

  return params;
}
function refreshContent() {
  let params = getParams();
  if (params.hasOwnProperty("q")) {
    document.getElementById("query").value = params["q"];
  }
  if (params.hasOwnProperty("o")) {
    document.getElementById("sort").value = params["o"];
  }
  if (params.hasOwnProperty("order")) {
    document.getElementById("order").value = params["order"];
  }
}


function PopUpStudent(){
  document.addEventListener("click",e=>{
    if (e.target.matches(".student-item") || e.target.matches(".student-item *")){
      let $row = e.target.closest(".student-item")
      if ($row){
        let name = $row.querySelectorAll("td")[1].textContent;
        let age = $row.querySelectorAll("td")[2].textContent;
        let email = $row.querySelectorAll("td")[3].textContent;
        alert(`Información del alumno: \n\nNombre: ${name}\nEdad: ${age}\nCorreo: ${email}`)
      }
    }
  })
}
function Order() {
  document.addEventListener("click", e => {
    if (e.target.matches("tr th")) {
      let $th = e.target;
      let thText = $th.textContent.toLowerCase();
      let isAsc = $th.dataset.order === "asc"; // Verifica si el orden actual es ascendente

      // Cambia el valor del atributo `data-order`
      $th.dataset.order = isAsc ? "desc" : "asc"; // Alterna entre "asc" y "desc"

      if (thText == "nombre" || thText == "edad" || thText == "correo") {
        let $rows = Array.from(document.querySelectorAll(".student-item"));

        // Ordenar por nombre (segundo <td>)
        if (thText == "nombre") {
          $rows.sort((a, b) => {
            let nameA = a.querySelectorAll("td")[1].textContent.toLowerCase();
            let nameB = b.querySelectorAll("td")[1].textContent.toLowerCase();

            // Cambiar el orden según sea ascendente o descendente
            return isAsc ? nameA.localeCompare(nameB) : nameB.localeCompare(nameA);
          });
        }

        // Ordenar por edad (tercer <td>), tratado como número
        if (thText == "edad") {
          $rows.sort((a, b) => {
            let ageA = parseInt(a.querySelectorAll("td")[2].textContent);
            let ageB = parseInt(b.querySelectorAll("td")[2].textContent);

            // Cambiar el orden según sea ascendente o descendente
            return isAsc ? ageA - ageB : ageB - ageA;
          });
        }

        // Ordenar por correo (cuarto <td>)
        if (thText == "correo") {
          $rows.sort((a, b) => {
            let emailA = a.querySelectorAll("td")[3].textContent.toLowerCase();
            let emailB = b.querySelectorAll("td")[3].textContent.toLowerCase();

            // Cambiar el orden según sea ascendente o descendente
            return isAsc ? emailA.localeCompare(emailB) : emailB.localeCompare(emailA);
          });
        }

        // Reinsertar las filas ordenadas en el tbody
        let $tbody = document.querySelector("tbody");
        $rows.forEach(row => $tbody.appendChild(row));
      }
    }
  });
}
