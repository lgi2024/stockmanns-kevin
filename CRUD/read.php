<?php
    include './utils/connect-db.php';
    require './services/user-services.php';
    $userService = new UserService($con);
    
    if(isset($_GET['logout'])){
        $userService->logout();
        exit();
    }

    if($userService->redirectIfNotLogin())
        exit();
?>


<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CRUD (Read)</title>
    <link rel="stylesheet" href="css/index.css">

    <style>
        header{
            position: fixed;
            top: 0;
            left: 0;
            padding: 1rem;
            display: grid;
            place-items: center;
            width: 100%;
            background: #fff;
            box-shadow: 0 1px 1px 1px #0005;
            min-height: 60px;
        }
        .search{
            display: flex;
            gap: .5rem;
            align-items: center;
            padding: .5rem;
        }
        .search input, .search select{
            padding: .3rem;
            font-size: 1rem;
            font-family: inherit;
        }
        main{
            margin-top: calc(60px + 4rem);
            display:grid;
            gap:1rem;
        }
        main > .btn{
            justify-self: center;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            box-shadow: 0 2px 3px rgba(0, 0, 0, 0.1);
            background-color: white;
            margin-bottom: 20px;
        }

        th, td {
            padding: 12px 15px;
            text-align: left;
            vertical-align: middle;
        }

        th {
            background-color: var(--mainColor);
            color: white;
            text-transform: uppercase;
            font-size: 14px;
            cursor: pointer;
        }
        td img{
            width: 4rem;
            height: 4rem;
            object-fit: cover;
        }

        tr {
            align-items: center;
            border-bottom: 1px solid #dddddd;
        }

        tr:nth-child(even) {
            background-color: #f3f3f3;
        }

        tr:hover {
            background-color: #f1f1f1;
        }

        tr:last-of-type {
            border-bottom: 2px solid var(--mainColor);
        }

        caption {
            caption-side: bottom;
            text-align: right;
            padding: 10px;
            font-weight: bold;
            color: #666;
        }

        .acciones{
            display:flex;
            gap:.5rem;
            align-items:center;
            justify-content:center;
            width:100%;
        }
        
    </style>
</head>
<body>
    <header>
        <div class="search" id="search-container">
            <input id="query" name="query" type="text" placeholder="Buscar...">
            <label for="sort">Ordenar por:</label>
            <select name="sort" id="sort">
                    <option value="name" selected>Nombre</option>
                    <option value="age">Edad</option>
                    <option value="email">Correo</option>
            </select>
            <select name="order" id="order">
                <option value="ASC" selected>Ascendente</option>
                <option value="DESC">Descendente</option>
            </select>
            <div class="btn" id="queryBtn">
                <ion-icon name="search-outline"></ion-icon>
                <span>Buscar</span>
            </div>
        </div>
        <a href="read.php?logout=logout" class="btn red">Cerrar Sesión</a>
    </header>
    <main>
        <h1>Listado de alumnos</h1>
        <?php
            if($userService->isAdmin()){
                print '<a class="btn" href="create.php">
                    <ion-icon name="add-circle-outline"></ion-icon>
                    <span>Agregar Alumno</span>
                 </a>';
            }
        ?>
        <table>
            <thead>
                <tr>
                    <th></th>
                    <th>Nombre</th>
                    <th>Edad</th>
                    <th>Correo</th>
                    <?php if($userService->isAdmin()) print '<th>Acciones</th>'?>
                </tr>
            </thead>
            <tbdoy>
                <?php
                    include("./utils/connect-db.php");
                    include("./utils/create-tables.php");

                    $result=null;
                    $sql = "";
                    $order = 'ASC';
                    if(isset($_GET['order'])){
                        $order = strtoupper($_GET['order']);
                        $order = ($order == 'ASC' || $order == 'DESC') ? $order : 'ASC';
                    }

                    if(isset($_GET['q']))
                        $sql = "SELECT * FROM students WHERE name LIKE ? OR email LIKE ?";
                    else
                        $sql = "SELECT * FROM students";

                    if(isset($_GET['o']) && ($_GET['o'] == 'name' || $_GET['o'] == 'age' || $_GET['o'] == 'email')){
                        $o = $_GET['o'];
                        $sql .= " ORDER BY $o $order";
                    }else{
                        $sql .= " ORDER BY name $order";
                    }
    
                    $stmt = $con->prepare($sql);
                    if($stmt){
                        if(isset($_GET['q'])){
                            $q = $_GET['q'];
                            $searchTerm = "%$q%";
                            $stmt->bind_param("ss", $searchTerm, $searchTerm);
                        }
                        $stmt->execute();
                        $result = $stmt->get_result();
                    
                        if( $result->num_rows > 0){
                            foreach($result as $row){
                                if($row['profile'])
                                    $urlImg = "assets/images" . DIRECTORY_SEPARATOR . $row['profile'];
                                else
                                    $urlImg = "assets/images/no-img.jpeg";
                                print '
                                <tr class="student-item" data-order="desc">
                                    <td><img src="' . $urlImg . '" alt="Imagen de perfil"></td>
                                    <td>' . $row['name'] . '</td>
                                    <td>' . $row['age'] . '</td>
                                    <td>' . $row['email'] . '</td>
                                '; 

                                if($userService->isAdmin()){
                                    print '<td class="acciones">
                                        <a href="update.php?id=' . $row['id'] . '" class="btn">
                                            <ion-icon name="cloud-upload-outline"></ion-icon>
                                            <span>Actualizar</span>
                                        </a>
                                        <a href="delete.php?id=' . $row['id'] . '" class="btn">
                                        <ion-icon name="trash-outline"></ion-icon><span>Eliminar</span></a>
                                    </td>';
                                }
                                print '</tr>';
                            }
                        }else{
                            print '<p>No hay alumnos registrados.</p>';
                        }
                    }
                    $con->close();
                ?>
            </body>
        </table>
    </main>

    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
    <script src="js/js.js"></script>
    <script>
        document.addEventListener("DOMContentLoaded", ()=>{
            if(window.location.href.includes("?")){
                let $container = document.getElementById("search-container");
                let newDiv = document.createElement("div");
                newDiv.className = "btn red";
                newDiv.textContent = "Reiniciar";
                $container.insertAdjacentElement("beforeend", newDiv);
            }
        })

        document.addEventListener("click", e=>{
            if(e.target.matches(".btn.red")){
                window.location.href = window.location.origin + window.location.pathname
            }
        })
    </script>
</body>
</html>