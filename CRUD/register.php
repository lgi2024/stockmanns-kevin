<?php
    include './utils/connect-db.php';
    require './services/user-services.php';

    $userService = new UserService($con);
    if($userService->isLogin()){
        header('Location: read.php');
        $con->close();
        exit();
    }

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if(isset($_POST['name']) 
            && isset($_POST['email'])
            && isset($_POST['password'])
        ){
            $name = $_POST['name'];
            $email = $_POST['email'];
            $pass = $_POST['password'];
            if($name && $email && $pass){
                $res = $userService->signup($name, $email, $pass);
                    if($res === true){
                        header('Location: read.php');
                    }else{
                        header('Location: register.php?e='.$res);
                    }
            }else{
                header('Location: register.php?e=required_data');
            }
        }else{
            header('Location: register.php?e=required_data');
        }
        $con->close();
    }
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrarse</title>
    <link rel="stylesheet" href="css/index.css">
    <style>
        form{
            width: calc(100% - 2rem);
            max-width: 500px;
        }
    </style>
</head>
<body>
<form action="" method="POST">
        <h1>Registrarse</h1>
        <div class="form-div">
            <label for="name">Nombre</label>
            <input type="text" id="name" name="name">
            <p class="hidden input-error" id="error-name"></p>
        </div>
        <div class="form-div">
            <label for="email">Correo</label>
            <input type="email" id="email" name="email">
            <p class="hidden input-error" id="error-email"></p>
        </div>

        <div class="form-div">
            <label for="password">Clave</label>
            <input type="password" id="password" name="password">
            <p class="hidden input-error" id="error-password"></p>
        </div>

        <div class="form-div">
            <input id="form-submit" class="btn" type="submit" value="Registrarme">
            <p class="form-error hidden" id="form-error"></p>
        </div>
    </form>

    <script>
        document.addEventListener("DOMContentLoaded", e=>{
            let $formError = document.getElementById('form-error');

            let params = new URLSearchParams(window.location.search);
            let error = params.get('e');
            if(error){
                $formError.classList.remove('hidden');
                if(error == 'required_data'){
                    $formError.textContent = 'Todos los datos son necesarios.';
                }else if(error == 'user_email'){
                    $formError.textContent = 'El correo esta en uso';
                }else{
                    $formError.textContent = error;
                }
            }
        })
    </script>
</body>
</html>