<?php
    class FileUploader{
        private $basePath;
        private $allowedFileTypes;
        private $errorMessage;

        public function __construct($targetDir = "assets".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR, $allowedFileTypes = ["jpg", "jpeg", "png"]) {
            $this->basePath = $targetDir;
            $this->allowedFileTypes = $allowedFileTypes;
            $this->errorMessage = "";
        }

        public function upload($file, $fileName){
            if ($file['error'] !== UPLOAD_ERR_OK) {
                $this->errorMessage = "Error al subir el archivo. Código de error: " . $file['error'];
                return false;
            }

            $fileExt = pathinfo($file['name'], PATHINFO_EXTENSION);
            $fileName = uniqid('img_', true) . '.' . $fileExt;

            if(getimagesize($file["tmp_name"]) === false){
                $this->errorMessage = "El archivo no es una imagen.";
                return false;
            }
            if(!$this->createDir()){
                $this->errorMessage = "Error al crear la carpeta";
                return false;
            }
            if(!in_array($fileExt, $this->allowedFileTypes)) {
                $this->errorMessage = "Imagen invalida, solo se permite .png, .jpg y .jpeg";
                return false;
            }

            $filePath = $this->basePath . DIRECTORY_SEPARATOR . $fileName;

            if (move_uploaded_file($file["tmp_name"], $filePath)) {
                return $fileName;
            } else {
                $this->errorMessage = "Lo siento, hubo un error al subir el archivo.";
                return false;
            }
        }
        public function delete($fileName){
            $filePath = $this->basePath . DIRECTORY_SEPARATOR . $fileName;

            if (file_exists($filePath)) {
                if (unlink($filePath)) {
                    return true;
                } else {
                    $this->errorMessage = "Hubo un error al eliminar el archivo.";
                    return false;
                }
            }
        }


        private function createDir(){
            if (!is_dir($this->basePath)) {
                if (mkdir($this->basePath, 0755, true)) {
                    return true;
                } else {
                    return false;
                }
            }
            return true;
        }

        public function getErrorMessage(){
            return $this->errorMessage;
        }
    }
?>