<?php
    class UserService{
        private $con;

        public function __construct($con) {
            $this->con = $con;
            session_start(); // inicia una sesión para manejar a los usuarios
        }

        public function signup($nameUser, $emailUser, $passUser){
            if($this->getUser($emailUser) != null){
                return "El correo ya esta en uso";
            }
            $stmt = $this->con->prepare("INSERT INTO users (name, email, password) VALUES(?, ?, ?)");
            if($stmt){
                $stmt->bind_param("sss", $nameUser, $emailUser, $passUser);
                if($stmt->execute()){
                    $_SESSION['user_id'] = $stmt->insert_id;
                    $_SESSION['user_rol'] = 'user';
                    $_SESSION['user_name'] = $emailUser;
                    return true;
                }
            }
            return 'No se pudo registrar el usuario';
        }
        public function login($username, $password){
            $user = $this->getUser($username);
            if($user !== null){
                if($this->verifyPassword($user['password'], $password)){
                    $_SESSION['user_id'] = $user['id'];
                    $_SESSION['user_rol'] = $user['rol'];
                    $_SESSION['user_name'] = $user['email'];
                    return true;
                }else{
                    return 'Credenciales inválidas.';
                }
            }
            return 'No se encontro el usuario en la base de datos.';
        }
        public function logout(){
            session_unset();
            session_destroy();
            header('Location: index.php');
        }

        public function verifyPassword($originPass, $passTry){
            return $originPass == $passTry;
        }
        public function isLogin(){
            return isset($_SESSION['user_id']) && $_SESSION['user_id'] > 0;
        }

        public function getUser($username){
            $username = trim($username);
            $stmt = $this->con->prepare("SELECT * FROM users WHERE email = ?");
            if($stmt){
                $stmt->bind_param("s", $username);
                $stmt->execute();
                $result = $stmt->get_result();
                if($result->num_rows>0)
                    return $result->fetch_assoc();
            }
            return null;
        }


        public function redirectIfNotLogin(){
            if(!$this->isLogin()){
                header('Location: index.php?e=required_login');
            }
            return !$this->isLogin();
        }
        public function showErrorIfNotAdmin(){
            if($this->redirectIfNotLogin())
                return true;
            if(!$this->isAdmin()){
                print '<div style="height: 100vh; display: flex; flex-direction:column; justify-content:center; align-items:center; font-size: 1.2rem; padding: 0 1rem; text-align:center;">';
                print '<p> No tienes los permisos necesarios para navegar en esta página.</p>';
                print '<a href="read.php">Volver al inicio</a>';
                print '</div>';
            }
            return !$this->isAdmin();
        }
        public function isAdmin(){
            return strtolower($_SESSION['user_rol']) == 'admin';
        }
    }
?>