<?php
    include("./utils/connect-db.php");
    require ('./services/user-services.php');

    $userService = new UserService($con);
    if($userService->showErrorIfNotAdmin()){
        exit();
    }
    if(isset($_POST['name']) && isset($_POST['age']) && isset($_POST['email'])){
        $name = $_POST['name'];
        $age = $_POST['age'];
        $email = $_POST['email'];

        $imagen = null;
        if($_FILES['profile']['error'] == UPLOAD_ERR_OK){
            require './services/file-uploader.php';
            $fileUploader = new FileUploader();
            $file = $_FILES['profile'];
            $imagen = $fileUploader->upload($file, $_GET['id']);
            if(!is_string($imagen)){
                header('Location: update.php?error=error_image');
                exit();
            }
        }


        if($name && $age && $email){
            $stmt;
            if($imagen === null){
                $stmt = $con->prepare('UPDATE students SET name = ?, age = ?, email = ? WHERE id = ?');
                $stmt->bind_param('sisi', $name, $age, $email, $_GET['id']);
            }else{
                $stmt = $con->prepare('UPDATE students SET name = ?, age = ?, email = ?, profile = ? WHERE id = ?');
                $stmt->bind_param('sissi', $name, $age, $email, $imagen, $_GET['id']);
            }
            if($stmt){
                if($stmt->execute()){
                    header('Location: read.php');
                }else{
                    header('Location: update.php?id=' . $_GET['id'] . '&error=error');
                }
                $stmt->close();
            }
        }else{
            header('Location: update.php?id=' . $_GET['id'] . '&error=required_data');
        }

        $con->close();
    }
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CRUD (Update)</title>
    <link rel="stylesheet" href="css/index.css">
</head>
<body>
    <main>
        <h1>Actualizar Estudiante</h1>
        <form method="post" enctype="multipart/form-data">
            <?php

                if(!isset($_GET['id'])){
                    header('Location: read.php');
                    exit();
                }

                $stmt = $con->prepare("SELECT * FROM students WHERE id = ?");
                if($stmt){
                    $stmt->bind_param("s", $_GET["id"]);
                    $stmt->execute();
                    $result = $stmt->get_result();
                    if($result->num_rows > 0){
                        $row = $result->fetch_assoc();

                        print '<div class="form-div">
                            <label for="name">Nombre</label>
                            <input type="text" name="name" id="name" value="' . $row['name'] . '"/>
                            <p class="input-error" id="error-name"></p>
                        </div>';

                        print '<div class="form-div">
                            <label for="age">Edad</label>
                            <input type="number" name="age" id="age" value="' . $row['age'] . '"/>
                            <p class="input-error" id="error-age"></p>
                        </div>';

                        print '<div class="form-div">
                            <label for="email">Correo</label>
                            <input type="email" name="email" id="email" value="' . $row['email'] . '"/>
                            <p class="input-error" id="error-email"></p>
                        </div>';

                        print '<div class="form-div">
                            <label for="profile">Correo</label>
                            <input type="file" name="profile" id="profile"/>
                            <p class="input-error" id="error-profile"></p>
                        </div>';
                    }else{
                        print "<p>Ocurrio un error. Vuelve a intenarlo</p>";
                    }
                    $stmt->close();
                }

                $con->close();
            ?>

            <div class="form-div">
                <input type="submit" class="btn" value="Actualizar">
                <p class="form-error hidden" id="error-form"></p>
            </div>
        </form>
    </main>

    <script>
        const d =document;

        d.addEventListener("DOMContentLoaded",e=>{
            let hasError = new URLSearchParams(window.location.search).get("error");
            let $errorForm = d.getElementById("error-form");
            if(hasError == "required_data"){
                $errorForm.textContent = 'Todos los campos son requeridos.';
                $errorForm.classList.remove("hidden");
            }else if(hasError == 'error_image'){
                $errorForm.textContent = 'Ocurrio un error al subir la imagen.';
                $errorForm.classList.remove("hidden");
            }else if(hasError == "error"){
                $errorForm.textContent = 'Ocurrio un error.Intentalo nuevamente';
                $errorForm.classList.remove("hidden");
            }
        });
    </script>
</body>
</html>