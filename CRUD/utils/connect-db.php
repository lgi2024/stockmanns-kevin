<?php
    $host = getenv('SQL_HOST');
    $username = getenv('SQL_USER');
    $password = getenv('SQL_PASSWORD');
    
    $con = mysqli_connect($host, $username, $password, "school");
    if(!$con) {
        die("Error al conectar db". mysqli_connect_error());
    }
?>