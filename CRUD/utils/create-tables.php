<?php
    $sqlStudents = "CREATE TABLE IF NOT EXISTS students(
        id INT PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(100) NOT NULL,
        age INT NOT NULL,
        email VARCHAR(255) NOT NULL,
        profile VARCHAR(255)
    )";

    mysqli_query($con, $sqlStudents);

    $sqlUsers = "CREATE TABLE IF NOT EXISTS users(
        id INT PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(100) NOT NULL,
        email VARCHAR(255) NOT NULL,
        password VARCHAR(100) NOT NULL,
        rol ENUM('admin', 'user') NOT NULL DEFAULT 'user'
    )";

    mysqli_query($con, $sqlUsers);
?>