# PEDIDOS

`formulario_pedidos` es una aplicación web diseñada para gestionar pedidos de productos. Este proyecto implementa un sistema de manejo de sesión de usuarios que permite a los usuarios autenticados interactuar con el sistema de acuerdo con su rol. Dependiendo del rol asignado, los usuarios tienen diferentes permisos y capacidades dentro de la aplicación:

- **Roles de Usuario**:
    - **Usuario**: Pueden realizar pedidos de productos disponibles.
    - **Administrador**: Pueden actualizar los productos en el sistema.


# CRUD

`CRUD` es una aplicación web diseñada para gestionar la información de los estudiantes. Este proyecto implementa un sistema completo de gestión de estudiantes, permitiendo crear, leer, actualizar y eliminar registros de estudiantes en una base de datos.

## Funcionalidades

- **Crear Estudiante**: Permite agregar un nuevo estudiante a la base de datos. El formulario de creación incluye campos para el nombre, la edad, el correo electrónico y la foto de perfil del estudiante.
- **Leer Estudiantes**: Muestra una lista de todos los estudiantes registrados en la base de datos.
- **Buscar y Ordenar Estudiantes**: Ofrece la capacidad de buscar estudiantes y ordenar la lista de resultados según criterios específicos como nombre, edad, o correo.
- **Actualizar Estudiante**: Permite modificar la información de un estudiante existente, incluida la actualización de la foto de perfil.
- **Eliminar Estudiante**: Permite eliminar un registro de estudiante de la base de datos.





