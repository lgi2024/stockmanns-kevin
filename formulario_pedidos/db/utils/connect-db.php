<?php
    $host = getenv('SQL_HOST');
    $username = getenv('SQL_USER');
    $password = getenv('SQL_PASSWORD');

    $con = mysqli_connect($host, $username, $password, "db_prueba");
    if (!$con) {
        die("Connection failed: " . mysqli_connect_error()); //devuelve un mensaje de error en caso de que no funcione
    }
?>