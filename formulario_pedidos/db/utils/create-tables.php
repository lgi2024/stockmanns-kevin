<?php
    $sqlProductos = 'CREATE TABLE IF NOT EXISTS productos (
        id INT AUTO_INCREMENT PRIMARY KEY,
        nombre VARCHAR(255) NOT NULL,
        descripcion TEXT,
        precio DECIMAL(10, 2) NOT NULL
    )';

    $sqlUsuarios = 'CREATE TABLE IF NOT EXISTS usuarios(
        id INT AUTO_INCREMENT PRIMARY KEY,
        nombre VARCHAR(100) NOT NULL,
        apellido VARCHAR(100) NOT NULL,
        clave VARCHAR(255) NOT NULL,
        username VARCHAR(20) NOT NULL,
        rol VARCHAR(20) NOT NULL
    )';
    
    mysqli_query($con, $sqlProductos);
    mysqli_query($con, $sqlUsuarios);
?>