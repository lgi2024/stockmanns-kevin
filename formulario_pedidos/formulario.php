<?php
  include './db/utils/connect-db.php';
  include './db/utils/create-tables.php';

  if(!isset($_POST['username']) || !isset($_POST['password'])){
    header('Location: index.html?error=error');
    exit();
  }

  $stmt = $con->prepare("SELECT clave, nombre, apellido, rol FROM usuarios WHERE username = ?");
    if ($stmt) {
      // Enlazar el parámetro
      $stmt->bind_param("s", $_POST['username']);
      
      // Ejecutar la consulta
      $stmt->execute();
      $result = $stmt->get_result();
      
      if ($result->num_rows > 0) {
        $usuario = $result->fetch_assoc();

        // Verificar la contraseña
        if ($_POST['password'] != $usuario['clave']) {
          header("Location: index.html?error=invalid_password");
          exit();
        }
      } else {
        header("Location: index.html?error=user_not_found");
        exit();
      }
      $stmt->close();
    }

?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Realizar Pedido</title>

    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/formulario.css">
  </head>
  <body>
    <main>
      <form action="pedido.php" method="post" class="form content">
        <?php
          print ('<p><b>Hola ' . $usuario['nombre'] . '.</b> Estos son los productos disponibles</p>')
        ?>

        <h3>Lista de Productos</h3>

        <?php

          $result = mysqli_query($con, "SELECT id, nombre, precio FROM productos");

          if($usuario['rol'] == 'admin'){
            if(mysqli_num_rows($result)>0){
              while($row = mysqli_fetch_assoc($result)){
                print '<div class="producto">
                  <span>'.$row['nombre'] .'</span>'
                  . '<span> $' . $row['precio'] . '</span>
                  <a class="btn" href="update-product.php?id=' . $row['id'] . '">Actualizar</a>
                </div>';
              }
            }else{
              print '<p>No hay productos que mostrar</p>';
            }
          }else{
            if(mysqli_num_rows($result)>0){
              while($row = mysqli_fetch_assoc($result)) {
                print '
                 <div>
                  <input type="checkbox" value="' . $row['nombre'] . '_' . $row['precio'] . '" id="' . $row['id'] . '" name="pedidos[]"/>
                  <label for="' . $row['id'] . '">' . $row['nombre'] . ' <b>(Costo $' . $row['precio'] . ')</b></label>
                </div>';
              }

              print '<div>
                <label for="modo-retiro">Tipo de entrega:</label>
                <select name="entrega" id="modo-retiro">
                  <option value="delivery" selected>Delivery (adicional 10%)</option>
                  <option value="retira">Retirar</option>
                </select>
              </div>';
              print '<input class="btn" type="submit" value="Realizar Pedido" />';
            } else {
              echo "<p>No hay productos que mostrar</p>";
          }
  
          $con->close();
          }
        ?>
      </form>
    </main>
  </body>
</html>
