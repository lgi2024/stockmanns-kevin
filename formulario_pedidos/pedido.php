<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Resumen del Pedido</title>
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/pedido.css">
</head>
<body>
    <main>
        <div class="content">
            <?php 
                $pedidos = $_POST['pedidos'];
                if($pedidos && $_POST['entrega']){
                        $suma = 0;

                        print '<h2>Pedido realizado con éxito</h2>';
                        print '<ul>';
                        for($i = 0; $i < count($pedidos); $i++){
                            $item = $pedidos[$i];
                            $precio = explode('_', $item)[1];
                            $prod = explode('_', $item)[0];
                            $suma = $suma + $precio;

                            print '<li>' . ucfirst($prod). ' ($' . $precio . ')';
                        }
                        print '</ul>';

                        print '<p>Precio del pedido: $' . $suma . '</p>';
                        if($_POST['entrega'] == 'delivery'){
                            print '<p> El pedido se enviará por delivery (adicional 10%)</p>';
                            $suma = $suma *1.1;
                        }else{
                            print '<p>El pedido se retirará por el cliente</p>';
                        }

                        print '<p><b>Precio Final: $' . $suma . '</b></p>';
                }else{
                    print '<p class="error">Los datos son requeridos</p>';
                }
            ?>
        </div>
    </main>
</body>
</html>