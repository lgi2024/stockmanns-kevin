<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Actualizar Producto</title>
    <link rel="stylesheet" href="./css/index.css">

    <style>
        main{
            width:100%;
            height:100vh;
        }
        .update-product{
            width:100%;
            max-width:500px;
        }
    </style>
</head>
<body>
    <main>
        <section class="update-product content">
            <h1>Actualizar Producto</h1>
            <form method="POST">
                <?php
                    include './db/utils/connect-db.php';
                    
                    if(isset($_POST['nombre']) && isset($_POST['precio'])){
                        print "<p>Actualizando producto...</p>";
                        if(!$_POST['nombre'] || !$_POST['precio']){
                            header('Location: update-product.php?id='.$_GET['id'].'&error=empty');
                            exit();
                        }else{
                            $stmt = $con->prepare("UPDATE productos SET nombre = ?, precio = ? WHERE id = ?");
                            if($stmt){
                                $stmt->bind_param("sss", $_POST["nombre"], $_POST['precio'], $_GET['id']);
                                $stmt->execute();

                                if($stmt->affected_rows> 0){
                                    print '<p class="succes">Producto actualizado con éxito. <a href="index.html">Volver al inicio</a></p>';
                                }else{
                                    print '<p>Ocurrio un error al actualizar el producto.</p>';
                                }
                            }
                        }
                    }else{
                        $stmt = $con->prepare("SELECT * FROM productos WHERE id = ?");
                        if($stmt){
                            $stmt->bind_param('s', $_GET['id']);
                            $stmt->execute();
                            $result = $stmt->get_result();
    
                            if($result->num_rows > 0){
                                $row = $result->fetch_assoc();
    
                                print '
                                <div class="form-div">
                                    <label for="nombre">Nombre</label>
                                    <input type="text" id="nombre" name="nombre" value="' . $row['nombre'] . '"/>
                                </div>
                                ';
    
                                print '
                                <div class="form-div">
                                    <label for="precio">Precio</label>
                                    <input type="number" id="precio" name="precio" value="' . $row['precio'] . '"/>
                                </div>
                                ';

                                print '<div class="form-div">
                                    <input class="btn" type="submit" value="Actualizar"/>
                                </div>';
                                print '<p class="form-error" id="form-error"></p>';
                                $stmt->close();
                            }
                        }
                    }
                ?>
                
            </form>
        </section>
    </main>

    <script>
        document.addEventListener("DOMContentLoaded", e=>{
            let $formError = document.getElementById('form-error');
            let hasError = new URLSearchParams(window.location.search).get('error');
            if(hasError=="empty"){
                $formError.textContent = 'Debes enviar todos los campos requeridos para actualziar';
                $formError.classList.remove('hidden');
            }else{
                $formError.classList.add('hidden');
            }
        });
    </script>
</body>
</html>